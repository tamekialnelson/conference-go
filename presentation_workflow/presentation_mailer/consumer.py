import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

while True:
    try:
        connection = pika.BlockingConnection(
            pika.ConnectionParameters("rabbitmq")
        )
        channel = connection.channel()
        channel.queue_declare(queue="presentation_approvals")
        channel.queue_declare(queue="presentation_rejections")

        def process_rejection(ch, method, properties, body):
            json_data = json.loads(body)
            presenter_name = json_data["presenter_name"]
            presenter_email = json_data["presenter_email"]
            title = json_data["title"]
            send_mail(
                [presenter_email],
                "From: admin@conference.go",
                "Subject: Your presentation has been rejected",
                f"{presenter_name}, we regret to inform",
                f"you that your presentation {title}",
                "has been rejected",
                fail_silently=False,
            )

        def process_approval(ch, method, properties, body):
            json_data = json.loads(body)
            presenter_name = json_data["presenter_name"]
            presenter_email = json_data["presenter_email"]
            title = json_data["title"]
            send_mail(
                [presenter_email],
                "From: admin@conference.go",
                "Subject: Your presentation has been accepted",
                f"{presenter_name}, we're happy to tell",
                f"you that your presentation {title}",
                "has been accepted",
                fail_silently=False,
            )
        channel.basic_consume(
            queue='presentation_approvals',
            on_message_callback=process_approval,
            auto_ack=True,
        )
        channel.start_consuming()
        channel.basic_consume(
            queue='presentation_rejections',
            on_message_callback=process_rejection,
            auto_ack=True,
        )
        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
